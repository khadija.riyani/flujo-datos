import java.util.Scanner;

public class FlujoDatosJava_6 {

	public static void main(String[] args) {
		/*Lee un n�mero por teclado que pida el precio de un producto (puede tener decimales) y
		calcule el precio final con IVA. El IVA sera una constante que sera del 21%*/
		
		Scanner teclat = new Scanner (System.in);
		
		double producto;
		System.out.println("introduce el precio del producto");
		producto = teclat.nextDouble();
		
		producto = producto + (producto * 21 /100);
		
		System.out.println("EL precio final del producto es : " + producto);
		
		
	}

}
