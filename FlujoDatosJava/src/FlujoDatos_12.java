import java.util.Scanner;

public class FlujoDatos_12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	 /*Escribe una aplicaci�n con un String que contenga una 
	contrase�a cualquiera. Despu�s se te pedir� que introduzcas la contrase�a, 
	con 3 intentos. Cuando aciertes ya no pedir� mas la contrase�a y mostrara un 
	mensaje diciendo �Enhorabuena�. Piensa bien en la condici�n de salida 
	(3 intentos y si acierta sale, aunque le queden intentos).*/
		
		
		Scanner teclat= new Scanner (System.in);
		
			//declaramos la variable inicializada con nuestra contrase�a
		
	       String contrase�a_correcta = "khadija";
	       String contrase�a; //la contrase�a que usara el usuario
	       boolean si = false;
	       
	        for (int i = 0; i < 3 && si==false; i++) {
	        	
	            System.out.print("Introduzca la contrase�a : ");
	            contrase�a = teclat.next(); //pedimos la contrase�a al usuario
	            
		            if (contrase�a.equals(contrase�a_correcta)){ //si la contrase�a es igual a la clave que pusimos 
		            	
		            System.out.println("Enhorabuena"); //nos saldra enhorabuena
		            	si=true;
		            }
		            else{ //sino habra que intentarlo de nuevo, maximo dos veces mas
		                System.out.println("ERROR, la contrase�a es incorrecta :( ");
		            }
	        }
	}

}
