import java.util.Scanner;

public class FlujoDatosJava_4 {

	public static void main(String[] args) {
		/*Haz una aplicaci�n que calcule el �rea de un circulo (pi*R2).
		 El radio se pedir� por teclado
		(recuerda pasar de String a double con Double.parseDouble). Usa la constante PI y el
		m�todo pow de Math.*/

		Scanner  teclat=new  Scanner (System.in);
		
		double radio;
		double area;
		
		System.out.println("introduce el valor del radio");
		radio = teclat.nextDouble();
		
		area = Math.PI * Math.pow(radio,2);
		
		System.out.println("El area del circulo es : "+ area);
	}

}
