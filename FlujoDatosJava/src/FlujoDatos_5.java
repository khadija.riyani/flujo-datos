import java.util.Scanner;

public class FlujoDatos_5 {

	public static void main(String[] args) {
		/*Lee un n�mero por teclado e indica si es divisible entre 2 (resto = 0). Si no lo es, tambi�n
		debemos indicarlo.*/
		
		Scanner teclat = new Scanner (System.in);
		
		int numero;
		
		System.out.println("introduce el numero");
		numero = teclat.nextInt();
		
		if (numero % 2 == 0) {
			System.out.println("Es divisible entre dos");
		}
		else {
			System.out.println("No es divisible entre dos");
		}
	}

}
