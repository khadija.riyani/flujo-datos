
public class FlujoDatos_9 {

	public static void main(String[] args) {
		/*Muestra los n�meros del 1 al 100 (ambos incluidos) divisibles entre 2 y 3. Utiliza el bucle
		que desees.
		*/
		int i;

		for (i = 0; i <= 100; i ++) {
			if ( i % 2 == 0 || i % 3 == 0) {
				System.out.println(i);
			}
			/*else {
				System.out.println(i + " no es multiple de 2 y 3");
			}*/
		}
	}

}
